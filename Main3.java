package ru.unn.lesson1;

import java.util.Arrays;
import java.util.Scanner;

public class Main3 {
//Полностью готова

    public static void main(String[] args) {
        System.out.println("Введите текст: ");
        Scanner in = new Scanner(System.in);
        String inputString = in.nextLine();
        System.out.println("Текст наоборот: "+ inversion(inputString));
    }

    public static String inversion(String inputString) {
        String rightPart;
        String leftPart;
        int length = inputString.length();
        if (length <= 1) {
            return inputString; }
        leftPart = inputString.substring(0, length / 2);
        rightPart = inputString.substring(length / 2, length);
        return inversion(rightPart) + inversion(leftPart);
    }
}